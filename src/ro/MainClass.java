package ro;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MainClass {

	private final static DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");

	/**
	 * List of patterns for which we want to determine the class.
	 */
	private final static Double[][] x = new Double[][] { 
		{1.0, 4.0, 0.0},
		{3.0, 5.0, 0.0}
	};

	public static void main(String[] args) {
		String[][] learningSet = FileUtils.readStrLearningSetFromFile("in.txt");

		int numberOfPatterns = learningSet.length;
		int numberOfFeatures = learningSet[0].length - 1;

		System.out.println(String.format("The learning set has %s patterns and %s features", 
				numberOfPatterns, numberOfFeatures));

		// Add to list all distinct class
		List<String> classesList = new ArrayList<String>();
		for (int i = 0; i < numberOfPatterns; i++) {
			String clazz = learningSet[i][learningSet[0].length - 1];

			if (!classesList.contains(clazz)) {
				classesList.add(clazz);
			}
		}

		System.out.println("Number of distinct classes: " + classesList.size());

		/*
		 * Declare w matrix having the number of distinct classes rows and the
		 * number of features + 1 columns.
		 */
		double[][] wMatrix = new double[classesList.size()][numberOfFeatures + 1];

		// Iterate each row of w matrix
		for (int i = 0; i < wMatrix.length; i++) {
			double freeTermSum = 0.0;

			// Iterate each column of w matrix
			for (int j = 0; j < wMatrix[0].length - 1; j++) {
				double totalSum = 0.0;
				int totalElements = 0;

				/**
				 * Iterate each feature for column we are currently parsing,
				 * if class matches the class for current w matrix row, add
				 * the value to sum and keep track of how many features we had.
				 */
				for (int i2 = 0; i2 < learningSet.length; i2++) {
					if (learningSet[i2][numberOfFeatures].equals(classesList.get(i))) {
						totalSum += Double.valueOf(learningSet[i2][j]);
						totalElements++;
					}
				}

				wMatrix[i][j] = totalSum / totalElements; // feature average
				freeTermSum += Math.pow(wMatrix[i][j], 2); 
			}

			wMatrix[i][wMatrix[i].length - 1] = -0.5 * freeTermSum; // free term
		}

		// Output nicely formated w matrix
		for (int i = 0; i < wMatrix.length; i++) {
			for (int j = 0; j < wMatrix[0].length; j++) {
				System.out.print(decimalFormat.format(wMatrix[i][j]) + " ");
			}

			System.out.println();
		}

		Double sigma = 0.0;
		int clasa = 0;

		// For each required pattern which we want to calculate the class for
		for(int k = 0; k < x.length; k++) {
			// Calculate discriminant functions (sigma) for each distinct class
			for (int i = 0; i < wMatrix.length; i++) {
				Double sigma1 = wMatrix[i][0] * x[k][0] + 
						wMatrix[i][1] * x[k][1] + 
						wMatrix[i][2] * x[k][2];

				if (sigma1 > sigma) {
					sigma = sigma1;
					clasa = i + 1;
				}
			}

			System.out.println("Class: " + clasa + " for pattern (" + x[k][0] 
					+ ", " + x[k][1] + ")");
		}
	}
}